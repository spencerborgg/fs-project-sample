import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import './Navbar.css'

import auth0Client from '../auth'

function Navbar(props) {
    const signOut = () => {
        auth0Client.signOut()
        props.history.replace('/')
    }

    return (
        <nav className="LetsSeeWhatIAm">
            <Link className='navbar' to="/users">
                Users
            </Link>
            <Link className='navbar' to="/forums">
                Forums
            </Link>
            {!auth0Client.isAuthenticated() && (
                <button className="btn btn-primary" onClick={() => {
                    auth0Client.signIn()
                }}>
                    Sign In
                </button>
            )}
            {auth0Client.isAuthenticated() && (
                <div>
                    <label htmlFor="mr-2 text-white">{auth0Client.getProfile().name}</label>
                    <button
                        className='btn btn-dark'
                        onClick={() => {
                            signOut()
                        }}
                    >
                        Sign Out
                    </button>
                </div>
            )}
        </nav>
    )
}

export default withRouter(Navbar)