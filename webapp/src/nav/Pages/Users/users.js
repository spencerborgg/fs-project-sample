import React, { Component } from 'react';
import authClient from '../../auth';


class Users extends Component {

    constructor(props) {
        super(props);
        this.state = {
            newUserEmail: '',
            newUserHandle: '',
            users: []
        };
        this.usersFetch = this.usersFetch.bind(this)
    }


    saveNewUser = event => {
        event.preventDefault();
        const newUser = { email: this.state.newUserEmail, handle: this.state.newUserHandle }
        fetch('http://localhost:8280/users', {
            method: 'POST',
            body: JSON.stringify(newUser),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(() =>
            this.usersFetch()
        )
    }

    removeUser(id) {
        fetch(`http://localhost:8280/users/${id}`, {
            method: 'DELETE'
        })
            .then(() => {
                this.usersFetch()
            })
    }

    updateUserInfo = event => {
        event.preventDefault();
        const updateUser = { email: this.state.newUserEmail, handle: this.state.newUserHandle }
        fetch(`http://localhost:8280/users`, {
            method: 'PUT',
            body: JSON.stringify(updateUser)
        })
    }

    usersFetch() {
        fetch('http://localhost:8280/users')
            .then(response => {
                return response.json();
            })
            .then(myJson => {
                console.log(JSON.stringify(myJson));
                this.setState({ users: myJson })
            })
    }

    usersList = () => {
        console.log(authClient.isAuthenticated())
        if (this.state.users) {
            return this.state.users.map((users, i) => {
                return (
                    <li key={i} >
                        user id: {users.id}<br />
                        Username:
            {users.handle}<br />
                        email:{users.email}<br />
                        {
                            authClient.isAuthenticated() &&
                            <button className='delete-btn'
                                onClick={() => { this.removeUser(users.id) }}>Delete</button>
                        }
                    </li>
                )
            })
        }
    }



    componentDidMount() {
        this.usersFetch()
    }



    render() {
        return (
            <div>
                here are the users: <ul>{this.usersList()}</ul>
                <div>become a user:<br />
                    Username <form onSubmit={this.saveNewUser}>
                        <input
                            onChange={(event) => this.setState({ newUserHandle: event.target.value })}
                            type='text'
                            name='userHandle'
                            value={this.state.newUserHandle} /><br />
                        email
            <br />
                        <input
                            onChange={(event) => this.setState({ newUserEmail: event.target.value })}
                            type='text'
                            name='email'
                            value={this.state.newUserEmail} /><br />
                        <input type='submit' value='Create User' />
                        <input type='submit' value='Update User' />
                    </form>
                </div>
            </div >
        );
    }
}

export default Users;
