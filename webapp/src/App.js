import React from 'react'
import { Route } from 'react-router-dom'


import Callback from './nav/callback'
import Users from './nav/Pages/Users/users';
import Navbar from './nav/navbar'
import Forums from './nav/Pages/Forums'

import './App.css'

function App() {
  return (
    <div className="App">
      <Navbar />
      <Route exact path="/forums" component={Forums} />
      <Route exact path="/callback" component={Callback} />
      <Route exact path="/users" component={Users} />
    </div>
  )
}

export default App