import dataFile from './users.json'

let data

const getData = () => {
  data = data || dataFile
  return Promise.resolve(data)
}

export default getData
