export default {
  client: 'pg',
  connection: process.env.DB_CONNECTION,
  migrations: {
    tableName: 'knex_migrations',
    directory: `${__dirname}/migrations`
  },
  pool: {
    min: 2,
    max: 10
  }
}
