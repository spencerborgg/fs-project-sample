export const up = knex => {
    console.log('running 00001 migration')
    return Promise.all(
        [
            knex.schema.hasTable('threads').then(exists => {
                if (!exists) {
                    return knex.schema.createTable('threads', table => {
                        table.uuid('id').primary()
                        table.string('subject', 100)
                        table.specificType('created_date', 'timestamptz').notNullable()
                        table.foreign('user_handle').references('handle').inTable('user')
                    })
                }
            })
        ])
}

export const down = knex => {
    return Promise.all([
        knex.sschema.dropTable('threads')
    ])
}